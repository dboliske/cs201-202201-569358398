# Lab 4

## Total

29/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        8/8
  * Application Class   1/1
* Documentation         3/3

## Comments

Nearly perfect, but your non-default constructor in your GeoLocation class doesn't do any validation.
